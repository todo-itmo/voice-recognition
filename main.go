package main

import (
	"github.com/gin-gonic/gin"
	"github.com/gnatunstyles/voice-recognition/internal/eureka"
	"github.com/gnatunstyles/voice-recognition/internal/routes"
)

func main() {
	eureka.InitEureka()
	r := gin.Default()
	routes.InitRoutes(r)
	r.Run(":8088")
}
