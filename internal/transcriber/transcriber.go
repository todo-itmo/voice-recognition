package transcriber

import (
	"bytes"
	"encoding/json"
	"fmt"
	"github.com/gin-gonic/gin"
	"io/ioutil"
	"mime/multipart"
	"net/http"
	"strconv"
	"strings"
	"time"
)

const (
	UploadUrl     = "https://api.assemblyai.com/v2/upload"
	TranscriptUrl = "https://api.assemblyai.com/v2/transcript"
)

type Event struct {
	Uid         string `json:"uid"`
	Name        string `json:"name"`
	Title       string `json:"title"`
	Note        string `json:"note"`
	IsCompleted bool   `json:"isCompleted"`
	Date        string `json:"date"`
	StartTime   string `json:"startTime"`
	EndTime     string `json:"endTime"`
	Color       int    `json:"color"`
	Category    string `json:"category"`
	Priority    string `json:"priority"`
	Latitude    int    `json:"latitude"`
	Longitude   int    `json:"longitude"`
}

type downloadForm struct {
	File *multipart.FileHeader `form:"file"`
}

func UploadFile(c *gin.Context, apiKey string) (string, error) {
	// Load file
	var formObj downloadForm
	if err := c.ShouldBind(&formObj); err != nil {
		c.String(http.StatusBadRequest, "bad request")
		return "", err
	}
	if err := c.ShouldBindUri(&formObj); err != nil {
		c.String(http.StatusBadRequest, "bad request")
		return "", err
	}

	err := c.SaveUploadedFile(formObj.File, fmt.Sprintf("records/%s", formObj.File.Filename))
	if err != nil {
		c.String(http.StatusInternalServerError, "unknown error")
		return "", err
	}

	//data, err := ioutil.ReadFile("test_voice_rec/test_voice.m4a")
	data, err := ioutil.ReadFile(fmt.Sprintf("records/%s", formObj.File.Filename))
	if err != nil {
		return "", err
	}

	// Setup HTTP client and set header
	client := &http.Client{}
	req, _ := http.NewRequest("POST", UploadUrl, bytes.NewBuffer(data))
	req.Header.Set("authorization", apiKey)
	res, err := client.Do(req)

	if err != nil {
		return "", err
	}

	defer res.Body.Close()

	// Decode json and store it in a map
	var result map[string]interface{}

	err = json.NewDecoder(res.Body).Decode(&result)
	if err != nil {
		return "", err
	}
	//fmt.Println("success")

	// Print the upload_url
	fmt.Println(result["upload_url"])

	return fmt.Sprintf("%v", result["upload_url"]), nil
}

func Transcribe(apiKey, audioUrl string) (string, error) {
	// Prepare json data
	values := map[string]string{
		"audio_url":     audioUrl,
		"language_code": "ru"}
	jsonData, err := json.Marshal(values)

	if err != nil {
		return "", err
	}

	// Setup HTTP client and set header
	client := &http.Client{}
	req, _ := http.NewRequest("POST", TranscriptUrl, bytes.NewBuffer(jsonData))
	req.Header.Set("content-type", "application/json")
	req.Header.Set("authorization", apiKey)
	res, err := client.Do(req)

	if err != nil {
		return "", err
	}

	defer res.Body.Close()

	// Decode json and store it in a map
	var result map[string]interface{}
	err = json.NewDecoder(res.Body).Decode(&result)
	if err != nil {
		return "", err
	}
	fmt.Println(result["id"])
	// Print the id of the transcribed audio
	return fmt.Sprintf("%v", result["id"]), nil
}

func GetText(apiKey, transcribedId string) (string, error) {
	pollingUrl := fmt.Sprintf("%s/%s", TranscriptUrl, transcribedId)
	time.Sleep(6 * time.Second)
	// Send GET request
	client := &http.Client{}
	req, _ := http.NewRequest("GET", pollingUrl, nil)
	req.Header.Set("content-type", "application/json")
	req.Header.Set("authorization", apiKey)
	res, err := client.Do(req)

	if err != nil {
		return "", err
	}

	defer res.Body.Close()

	var result map[string]interface{}

	err = json.NewDecoder(res.Body).Decode(&result)
	if err != nil {
		return "", err
	}
	fmt.Println(result)
	// Check status and print the transcribed text
	if result["status"] == "completed" {
		//fmt.Println("success")
		fmt.Println(result["text"])
	}
	return fmt.Sprintf("%v", result["text"]), nil
}

func ParseText(text string) Event {
	var result = Event{
		Uid:         "",
		Category:    "Personal",
		Priority:    "Medium",
		IsCompleted: false,
	}
	arrText := strings.Split(text, "на")

	from, to := parseTime(arrText[1])
	date := time.Now().Format("2006-01-02")

	result.StartTime = from
	result.EndTime = to
	result.Date = date

	if strings.Contains(text, "будильник") {
		result.Category = "Будильник"
	} else {
		result.Category = "Событие"
	}

	result.Title = strings.Trim(arrText[0], " ")

	return result
}

func parseTime(text string) (string, string) {
	arrText := strings.Split(text, "часов")
	hours := strings.Trim(arrText[0], " ")
	if hours == "" {
		return "00:00", "01:00"
	}
	hoursToInt, _ := strconv.Atoi(hours)
	hoursTo := fmt.Sprint(hoursToInt + 1)
	minutes := strings.ReplaceAll(
		strings.ReplaceAll(
			strings.ReplaceAll(arrText[1], ".", ""),
			" ", ""),
		"минут", "")

	if minutes == "" {
		minutes = "00"
	}

	return fmt.Sprintf("%s:%s", hours, minutes), fmt.Sprintf("%s:%s", hoursTo, minutes)
}
