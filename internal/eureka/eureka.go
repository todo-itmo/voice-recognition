package eureka

import (
	eureka "github.com/xuanbo/eureka-client"
)

func InitEureka() {
	client := eureka.NewClient(&eureka.Config{
		DefaultZone:           "http://eureka:8761/eureka/",
		App:                   "voice-recognition",
		Port:                  8088,
		RenewalIntervalInSecs: 10,
		DurationInSecs:        30,
		Metadata: map[string]interface{}{
			"VERSION":              "0.1.0",
			"NODE_GROUP_ID":        0,
			"PRODUCT_CODE":         "DEFAULT",
			"PRODUCT_VERSION_CODE": "DEFAULT",
			"PRODUCT_ENV_CODE":     "DEFAULT",
			"SERVICE_VERSION_CODE": "DEFAULT",
		},
	})
	client.Start()
}
