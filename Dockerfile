FROM golang:1.20

# Устанавливаем рабочую директорию
WORKDIR /voice-recognition

# Копируем исходный код в образ
COPY . .

# Запускаем go модуль и билдим приложение
RUN go mod tidy
RUN go build -o app

# Устанавливаем команду для запуска приложения
CMD ["./app"]
